//
//  ViewController.m
//  AOCollectionTabControllerDemo
//
//  Created by Anthony Miller on 5/8/14.
//  Copyright (c) 2014 Anthony Miller. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (instancetype)initWithTitle:(NSString *)title backgroundColor:(UIColor *)color
{
    self = [super init];
    if (self) {
        self.title = title;
        _backgroundColor = color;
    }
    return self;
}

- (void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    view.backgroundColor = self.backgroundColor;
    [self setView:view];
}

@end
