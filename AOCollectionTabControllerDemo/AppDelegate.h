//
//  AppDelegate.h
//  AOCollectionTabControllerDemo
//
//  Created by Anthony Miller on 5/8/14.
//  Copyright (c) 2014 Anthony Miller. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;
@end
