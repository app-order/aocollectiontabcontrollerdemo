//
//  AppDelegate.m
//  AOCollectionTabControllerDemo
//
//  Created by Anthony Miller on 5/8/14.
//  Copyright (c) 2014 Anthony Miller. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "AOCollectionTabController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setUpTabController];
    [self setupWindow];
    return YES;
}

- (void)setUpTabController
{
    NSArray *viewControllers = @[[[ViewController alloc] initWithTitle:@"Title 1" backgroundColor:[UIColor redColor]],
                                 [[ViewController alloc] initWithTitle:@"Title 2 - LONG TITLE" backgroundColor:[UIColor greenColor]],
                                 [[ViewController alloc] initWithTitle:@"Title 3" backgroundColor:[UIColor blueColor]],
                                     [[ViewController alloc] initWithTitle:@"Title 4" backgroundColor:[UIColor yellowColor]]];
    AOCollectionTabController *tabController = [[AOCollectionTabController alloc]
                                                initWithTitle:@"Tab Title"
                                                viewControllers:viewControllers];
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:tabController];
    self.navController.navigationBar.barStyle = UIBarStyleBlack;
}

- (void)setupWindow
{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = self.navController;
    [self.window makeKeyAndVisible];
}

@end
