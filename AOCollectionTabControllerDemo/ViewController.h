//
//  ViewController.h
//  AOCollectionTabControllerDemo
//
//  Created by Anthony Miller on 5/8/14.
//  Copyright (c) 2014 Anthony Miller. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) UIColor *backgroundColor;
- (instancetype)initWithTitle:(NSString *)title backgroundColor:(UIColor *)color;
@end
