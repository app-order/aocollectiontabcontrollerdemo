//
//  NSBundle+AOCellBundleName.m
//  AOCells
//
//  Created by Joshua Greene on 2/26/14.
//  Copyright (c) 2014 App-Order (http://app-order.com)



#import "NSBundle+AOCollectionTabControllerBundle.h"

NSString * const AOCollectionTabControllerBundleName = @"AOCollectionTabControllerBundle.bundle";

@implementation NSBundle (AOCollectionTabControllerBundle)

+ (NSBundle *)AOCollectionTabControllerBundle
{
    static NSBundle *bundle = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *mainBundlePath = [[NSBundle mainBundle] resourcePath];
        NSString *path = [mainBundlePath stringByAppendingPathComponent:AOCollectionTabControllerBundleName];
        bundle = [NSBundle bundleWithPath:path];
    });
    return bundle;
}

@end
