//
//  AOPageViewController.m
//  AOCore
//
//  Created by Joshua Greene on 11/21/13.
//  Copyright (c) 2013 App-Order. All rights reserved.
//

#import "AOPageViewController.h"

@interface AOPageViewController ()

@end

@implementation AOPageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = YES;
}

@end