//
//  AOCollectionLabelCell.m
//
//  Modified by Anthony Miller on 5/13/14
//  Created by Joshua Greene on 7/9/13.
//  Copyright (c) 2013 App-Order. All rights reserved.
//


#import "AOCollectionLabelCell.h"
#import "UIImage+Bundle.h"

@implementation AOCollectionLabelCell

#pragma mark - Object lifecycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
        selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        UIImage *backgroundImage = [[UIImage imageNamed:@"collection_view_cell_background" bundle:[NSBundle mainBundle]]
                                    resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, 3.0f, 19.0f, 3.0f)
                                    resizingMode:UIImageResizingModeStretch];
        
        self.imageView = [[UIImageView alloc] initWithFrame:selectedBackgroundView.bounds];
        [self.imageView setImage:backgroundImage];
        self.imageView.autoresizingMask = selectedBackgroundView.autoresizingMask;
        [selectedBackgroundView addSubview:self.imageView];
        [self setSelectedBackgroundView:selectedBackgroundView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected
{
    if (self.selected == selected)
        return;
    
    [super setSelected:selected];
    self.imageView.hidden = !selected;
}

@end