//
//  AOCollectionSubViewController.h
//
//  Modified by Anthony Miller on 5/13/14
//  Created by Joshua Greene on 7/9/13.
//  Copyright (c) 2013 App-Order. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AOCollectionTabController;

@protocol AOCollectionSubViewController <NSObject>
@required
@property (nonatomic, weak) AOCollectionTabController *collectionViewController;
@property (nonatomic) BOOL hideNavigationBar;
- (BOOL)showActionButton;
- (void)collectionActionButtonPressed;
- (UIImage *)actionButtonImage;
@optional
- (NSString *)actionButtonTitle;
@end