//
//  NSBundle+AOCellBundleName.h
//  AOCollectionTabController
//
//  Created by Joshua Greene on 2/26/14.
//  Copyright (c) 2014 App-Order (http://app-order.com)


#import <Foundation/Foundation.h>

@interface NSBundle (AOCollectionTabControllerBundle)
+ (NSBundle *)AOCollectionTabControllerBundle;
@end
