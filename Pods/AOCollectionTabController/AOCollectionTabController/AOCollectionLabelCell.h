//
//  AOCollectionLabelCell.h
//  AOCore
//
//  Created by Joshua Greene on 7/9/13.
//  Copyright (c) 2013 App-Order. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOCollectionLabelCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIColor *selectedTextColor;
@end