//
//  AOCollectionTabController.h
//  
//  Modified by Anthony Miller on 5/13/14
//  Created by Joshua Greene on 7/9/13.
//  Copyright (c) 2013 App-Order. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AOPageViewController.h"

@interface AOCollectionTabController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,
                                                         UIPageViewControllerDataSource, UIPageViewControllerDelegate>

// iVars
@property (nonatomic, readonly) NSInteger selectedTabIndex;
@property (nonatomic, strong) NSArray *viewControllers;
@property (nonatomic, strong) UIPageViewController *pageViewController;

// IBOutlets
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

// Configuration
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic) CGFloat cellSpacing;
@property (nonatomic) UIEdgeInsets edgeInsets;
@property (nonatomic, strong) UIColor *navBarColor;
@property (nonatomic, strong) UIColor *selectedBackgroundColor;

// Object lifecycle
- (instancetype)initWithTitle:(NSString *)title viewControllers:(NSArray *)viewControllers;
@end
