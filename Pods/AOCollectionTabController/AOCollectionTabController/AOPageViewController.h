//
//  AOPageViewController.h
//  AOCore
//
//  Created by Joshua Greene on 11/21/13.
//  Copyright (c) 2013 App-Order. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AOPageViewController : UIPageViewController

@end