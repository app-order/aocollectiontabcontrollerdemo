//
//  AOCollectionTabController.m
//
//  Modified by Anthony Miller on 5/13/14
//  Created by Joshua Greene on 7/9/13.
//  Copyright (c) 2013 App-Order. All rights reserved.
//

#import "AOCollectionTabController.h"

#import "AOCollectionLabelCell.h"
#import "AOCollectionSubViewController.h"
#import "UIImage+Bundle.h"

static NSString *AOCollectionLabelCellIdentifier = @"AOCollectionLabelCellIdentifier";

@interface AOCollectionTabController ()
@property (nonatomic, readwrite) NSInteger selectedTabIndex;

// Actions
- (void)showActionButton:(BOOL)shouldShow animated:(BOOL)animated image:(UIImage *)image orTitle:(NSString *)title;
- (void)reloadButtonPressed:(id)sender;
- (void)selectItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated;
- (void)updateCollectionItems:(UIViewController *)viewController animated:(BOOL)animated;
@end

@implementation AOCollectionTabController

#pragma mark - Object Lifecycle

- (instancetype)init
{
    return [self initWithTitle:nil viewControllers:nil];
}

- (instancetype)initWithTitle:(NSString *)title viewControllers:(NSArray *)viewControllers
{
    self = [super init];
    
    if (self)
    {        
        self.title = title;
        [self setViewControllers:viewControllers];
        _cellSpacing = 50.0f;
        _edgeInsets = UIEdgeInsetsMake(6.0f, 96.0f, 6.0f, 20.0f);
        _selectedBackgroundColor = [UIColor colorWithRed:134.0f green:134.0f blue:134.0f alpha:1.0];
    }
    return self;
}

#pragma mark - Custom Setters

- (void)setCellSpacing:(CGFloat)cellSpacing
{
    _cellSpacing = fmaxf(30.0f, cellSpacing);
}

- (void)setSelectedBackgroundColor:(UIColor *)selectedBackgroundColor
{
    if (_selectedBackgroundColor == selectedBackgroundColor)
        return;
    else if ([selectedBackgroundColor isEqual:[UIColor blackColor]])
        _selectedBackgroundColor = [UIColor colorWithRed:134.0f green:134.0f blue:134.0f alpha:1.0];
    else
        _selectedBackgroundColor = selectedBackgroundColor;
}

- (void)setViewControllers:(NSArray *)viewControllers
{
    if (_viewControllers == viewControllers)
        return;
    
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:viewControllers.count];
    
    for (UIViewController *viewController in viewControllers)
    {
        if ([viewController isKindOfClass:[UINavigationController class]])
        {
            [array addObject:viewController];
        }
        else
        {
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
            [array addObject:navigationController];
        }
    }
    
    _viewControllers = [array copy];
    
    for (UIViewController *viewController in viewControllers)
    {
        if ([viewController conformsToProtocol:@protocol(AOCollectionSubViewController)])
        {
            [(UIViewController<AOCollectionSubViewController> *)viewController setCollectionViewController:self];
        }
    }
}

#pragma mark - VIEW LIFECYCLE

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpCollectionViewLayout];
    [self registerCollectionViewCell];
    [self setUpPageViewController];
    
    [self selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)setUpCollectionViewLayout
{
    [(UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [(UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout setMinimumInteritemSpacing:self.cellSpacing];
    [(UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout setSectionInset:UIEdgeInsetsMake(0.0f, self.cellSpacing, 0.0f, self.cellSpacing)];
}

- (void)registerCollectionViewCell
{
    UINib *labelCellNib = [UINib nibWithNibName:@"AOCollectionLabelCell" bundle:[NSBundle mainBundle]];
    [self.collectionView registerNib:labelCellNib forCellWithReuseIdentifier:AOCollectionLabelCellIdentifier];
}

- (void)setUpPageViewController
{
    self.pageViewController = [[AOPageViewController alloc]
                               initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                               navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                               options:@{UIPageViewControllerOptionInterPageSpacingKey: @(10.0f)}];
    
    self.pageViewController.view.frame = self.contentView.bounds;
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    [self addChildViewController:self.pageViewController];
    [self.contentView addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (self.navBarColor && ![self.navBarColor isEqual:[UIColor whiteColor]])
    {
        self.navigationController.navigationBar.barTintColor = self.navBarColor;
    }
}

#pragma mark - Actions

- (void)showActionButton:(BOOL)shouldShow animated:(BOOL)animated image:(UIImage *)image orTitle:(NSString *)title
{        
    if (shouldShow)
    {
        UIBarButtonItem *barButton = nil;
        
        if (image)
        {
            barButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleBordered target:self action:@selector(reloadButtonPressed:)];
        }
        else if (title.length)
        {
            barButton = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleBordered target:self action:@selector(reloadButtonPressed:)];
        }
        else
        {
            image = [UIImage imageNamed:@"refresh_icon" bundle:[NSBundle mainBundle]];
            barButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleBordered target:self action:@selector(reloadButtonPressed:)];
        }
        
        [self.navigationItem setRightBarButtonItem:barButton animated:animated];
    }
    else if (!shouldShow)
    {
        [self.navigationItem setRightBarButtonItem:nil animated:animated];
    }
}

- (void)reloadButtonPressed:(id)sender
{
    UIViewController<AOCollectionSubViewController>  *viewController = self.viewControllers[self.selectedTabIndex];
    [viewController collectionActionButtonPressed];
}

- (void)selectItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated
{
    if (indexPath.row < 0 || indexPath.row >= self.viewControllers.count)
        return;
    
    self.selectedTabIndex = indexPath.row;
    [self.collectionView selectItemAtIndexPath:indexPath
                                      animated:animated
                                scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    
    [self setViewControllerForSelectedTabAnimated:animated];
}

- (void) setViewControllerForSelectedTabAnimated:(BOOL)animated
{
    UIViewController *viewController = self.viewControllers[self.selectedTabIndex];
    [self.pageViewController setViewControllers:@[viewController]
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO completion:nil];
    
    [self updateCollectionItems:viewController animated:animated];
}

- (void)updateCollectionItems:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([viewController conformsToProtocol:@protocol(AOCollectionSubViewController)])
    {
        BOOL show = [(UIViewController<AOCollectionSubViewController> *)viewController showActionButton];
        UIImage *image = [(UIViewController<AOCollectionSubViewController> *)viewController actionButtonImage];
        NSString *title = nil;
        
        if ([viewController respondsToSelector:@selector(actionButtonTitle)])
        {
            title = [(UIViewController<AOCollectionSubViewController> *)viewController actionButtonTitle];
        }
        
        [self showActionButton:show animated:animated image:image orTitle:title];
    }
    else
    {
        [self showActionButton:NO animated:animated image:nil orTitle:nil];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.viewControllers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AOCollectionLabelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:AOCollectionLabelCellIdentifier
                                                                            forIndexPath:indexPath];
    [self configureCollectionCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCollectionCell:(AOCollectionLabelCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [self setBackgroundColorForCell:cell];
    [self setTitleForCell:cell atIndexPath:indexPath];
    
    if (cell.selected == NO) {
        cell.imageView.hidden = YES;
    }

}

- (void)setBackgroundColorForCell:(AOCollectionLabelCell *)cell
{
    if (self.selectedBackgroundColor)
        cell.imageView.backgroundColor = self.selectedBackgroundColor;
}

- (void)setTitleForCell:(AOCollectionLabelCell *)cell  atIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController = self.viewControllers[indexPath.row];
    cell.label.text = [viewController.title uppercaseString];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectedTabIndex == indexPath.row)
        return;
    
    [self selectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UINib *labelCellNib = [UINib nibWithNibName:@"AOCollectionLabelCell" bundle:[NSBundle mainBundle]];
    AOCollectionLabelCell *sizingCell = [[labelCellNib instantiateWithOwner:nil options:nil] objectAtIndex:0];
    
    [self configureCollectionCell:sizingCell atIndexPath:indexPath];
    
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize sizingCellSize = [sizingCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    sizingCellSize.height = sizingCellSize.height + self.edgeInsets.bottom;
    return sizingCellSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section
{
    return self.edgeInsets;
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self.viewControllers indexOfObject:viewController];
    if (index != NSNotFound && index > 0)
        return self.viewControllers[index-1];
    
    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self.viewControllers indexOfObject:viewController];
    if (index < (self.viewControllers.count - 1))
        return self.viewControllers[index+1];
    
    return nil;
}

#pragma mark - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController
        didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIViewController *viewController = pageViewController.viewControllers[0];
        self.selectedTabIndex = [self.viewControllers indexOfObject:viewController];

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedTabIndex inSection:0];
        [self.collectionView selectItemAtIndexPath:indexPath
// TODO: Is there a reason this is set to NO? I think it looks better animated.
                                          animated:NO
                                    scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
        [self updateCollectionItems:viewController animated:YES];
    });
}

@end