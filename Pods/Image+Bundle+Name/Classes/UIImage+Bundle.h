//
//  UIImage+Bundle.h
//  AOCore
//
//  Created by App-Order on 2/17/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Bundle)
+ (UIImage *)imageNamed:(NSString *)name bundle:(NSBundle *)bundle;
@end
