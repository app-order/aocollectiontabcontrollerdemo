//
//  UIImage+Bundle.m
//  AOCore
//
//  Created by App-Order on 2/17/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "UIImage+Bundle.h"

@implementation UIImage (Bundle)

+ (UIImage *)imageNamed:(NSString *)name bundle:(NSBundle *)bundle
{
    if (!bundle || bundle == [NSBundle mainBundle])
        return [UIImage imageNamed:name];
    
    UIImage *image = [UIImage imageNamed:[self imageName:name forBundle:bundle]];
    return image;
}

+ (NSString *)imageName:(NSString *)name forBundle:(NSBundle *)bundle
{
    NSString *bundleName = [[bundle bundlePath] lastPathComponent];
    name = [bundleName stringByAppendingPathComponent:name];
    return name;
}

@end