
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AOCollectionTabController
#define COCOAPODS_POD_AVAILABLE_AOCollectionTabController
#define COCOAPODS_VERSION_MAJOR_AOCollectionTabController 0
#define COCOAPODS_VERSION_MINOR_AOCollectionTabController 1
#define COCOAPODS_VERSION_PATCH_AOCollectionTabController 0

// Image+Bundle+Name
#define COCOAPODS_POD_AVAILABLE_Image_Bundle_Name
#define COCOAPODS_VERSION_MAJOR_Image_Bundle_Name 0
#define COCOAPODS_VERSION_MINOR_Image_Bundle_Name 1
#define COCOAPODS_VERSION_PATCH_Image_Bundle_Name 0

